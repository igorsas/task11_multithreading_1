package com.igor.controller;

import com.igor.model.pingPong.Game;
import com.igor.model.pingPong.Player;
import com.igor.view.PingPongView;

import java.io.IOException;
import static com.igor.model.Constants.*;
import static com.igor.model.pingPong.PingPongConstants.*;

public class PingPongController implements Controller{
    private Player firstPlayer;
    private Player secondPlayer;
    public static PingPongView view;
    private static Game game;

    public void execute() {
        initializePlayers();
        view = new PingPongView(firstPlayer, secondPlayer);
        game = new Game(firstPlayer, secondPlayer);
        view.showInstruction();
        ThreadGroup players = new ThreadGroup(NAME_THREAD_FOR_PLAYERS);
        Thread firstPlayerThread = getFirstPlayerThread(firstPlayer, players);
        Thread secondPlayerThread = getSecondPlayerThread(secondPlayer, players);
        firstPlayerThread.start();
        secondPlayerThread.start();
        try {
            firstPlayerThread.join();
            secondPlayerThread.join();
        } catch (InterruptedException e) {
            logger.error(INTERRUPTED_EXCEPTION_MSG);
            e.printStackTrace();
        }
        Player winner = getWinner();
        view.congratulationsWinner(winner);
    }

    private Player getWinner() {
        return firstPlayer.getPoint() > secondPlayer.getPoint() ? firstPlayer : secondPlayer;
    }

    private Thread getFirstPlayerThread(Player firstPlayer, ThreadGroup threadGroup) {
        return new Thread(threadGroup, () -> {
            synchronized (SYNCHRONIZED_OBJECT) {
                while (noWinner()) {
                    try {
                        SYNCHRONIZED_OBJECT.wait();
                        view.out(STEP_PLAYERS + firstPlayer.getName());
                        game.doStep(firstPlayer);
                    } catch (InterruptedException e) {
                        logger.error(INTERRUPTED_EXCEPTION_MSG);
                        e.printStackTrace();
                    }
                    SYNCHRONIZED_OBJECT.notify();
                }
            }
        });
    }

    private Thread getSecondPlayerThread(Player secondPlayer, ThreadGroup threadGroup) {
        return new Thread(threadGroup, () -> {
            synchronized (SYNCHRONIZED_OBJECT) {
                while (noWinner()) {
                    SYNCHRONIZED_OBJECT.notify();
                    view.out(STEP_PLAYERS + secondPlayer.getName());
                    game.doStep(secondPlayer);
                    try {
                        SYNCHRONIZED_OBJECT.wait();
                    } catch (InterruptedException e) {
                        logger.error(INTERRUPTED_EXCEPTION_MSG);
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private boolean noWinner() {
        return Math.abs(firstPlayer.getPoint() - secondPlayer.getPoint()) < MIN_DIFF_FOR_WIN ||
                (firstPlayer.getPoint() < MIN_POINTS_FOR_WIN && secondPlayer.getPoint() < MIN_POINTS_FOR_WIN);
    }

    private void initializePlayers() {
        firstPlayer = createPlayer();
        secondPlayer = createPlayer();
    }

    private Player createPlayer() {
        String playerName = EMPTY_STRING;
        while (playerName.equals(EMPTY_STRING)) {
            logger.info(ASK_PLAYER_NAME);
            try {
                playerName = reader.readLine();
            } catch (IOException e) {
                logger.error(IOEXCEPTION_MSG);
                e.printStackTrace();
            }
        }
        return new Player(playerName);
    }
}
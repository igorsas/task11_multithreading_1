package com.igor.controller;

import com.igor.model.fibonacci.Fibonacci;
import com.igor.view.FibonacciView;

import java.io.IOException;

import static com.igor.model.Constants.*;
import static com.igor.model.fibonacci.FibonacciConstants.*;

public class FibonacciController implements Controller {
    private FibonacciView view = new FibonacciView();
    @Override
    public void execute() {

        Fibonacci fibonacci = new Fibonacci(getCountThreads());
        fibonacci.startAllThreads();
        fibonacci.joinToAllThreads();
        view.showFibonacciNumber(fibonacci.getFibonacciNumbers());
    }

    private int getCountThreads(){
        int countThreads = INCORRECT_COUNT_OF_THREADS;
        while (countThreads < MIN_COUNT_OF_THREADS) {
            logger.info(ASK_COUNT_OF_THREADS);
            try {
                countThreads = Integer.parseInt(reader.readLine());
            } catch (IOException e) {
                logger.error(IOEXCEPTION_MSG);
                e.printStackTrace();
            }
        }
        return countThreads;
    }
}

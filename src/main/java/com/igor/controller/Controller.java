package com.igor.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public interface Controller {
    Logger logger = LogManager.getLogger(Controller.class);
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    void execute();
}

package com.igor.view;

import com.igor.model.pingPong.Player;

import static com.igor.model.pingPong.PingPongConstants.*;
import static com.igor.model.Constants.*;
public class PingPongView implements View {
    private Player firstPlayer;
    private Player secondPlayer;

    public PingPongView(Player firstPlayer, Player secondPlayer) {
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
    }

    @Override
    public void out(String s) {
        logger.info(s);
    }

    public void showInstruction() {
        logger.info(INSTUCTION);
    }

    public void showScore() {
        logger.info(firstPlayer.getName() + GAP + firstPlayer.getPoint()
                + COLON_STRING + secondPlayer.getName() + GAP + secondPlayer.getPoint());
    }

    public void congratulationsWinner(Player winner) {
        logger.info(winner.getName() + CONGRATULATIONS_PLAYER);
    }
}

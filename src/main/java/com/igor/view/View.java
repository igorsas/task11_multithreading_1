package com.igor.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface View {
    Logger logger = LogManager.getLogger(PingPongView.class);

    default void out(String s) {
        logger.info(s);
    }
}

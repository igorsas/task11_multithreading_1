package com.igor.view;

import java.util.List;
import static com.igor.model.fibonacci.FibonacciConstants.*;
import static com.igor.view.View.logger;

public class FibonacciView {
    public void showFibonacciNumber(List<Long> fibonacciNumbers){
        logger.info(FIBONACCI_NUMBERS);
        for(Long number : fibonacciNumbers){
            logger.info(number);
        }
    }
}
package com.igor.model.pingPong;

public class PingPongConstants {
    public final static String INSTUCTION = "1. If someone kick with strong power, you can't kick also with strong power." +
            "\n2. If someone kick on the right, you have to defend on the left and via verse." +
            "\n3. For win you have to have minimum 11 points and on 2 points more then opponent";

    public final static String ASK_PLAYER_NAME = "Please enter your name: ";
    public final static String ASK_SIDE = ", please enter side : " +
            "\n1 - Left\n2 - Center\n3 - Right";
    public final static String KICK_STR = "Kick";
    public final static String DEFEND_STR = "Defend";
    public final static String ASK_POWER_OF_KICK = "Please enter power of kick (1 - 10)";
    public final static String CONGRATULATIONS_PLAYER = " Congratulation! You're Winner!";
    public final static String STEP_PLAYERS = "Step player's: ";
    public final static String SCORED = ", you scored";

    public final static String NAME_THREAD_FOR_PLAYERS = "Players";

    public final static int MIN_NUMBER_OF_POWER = 1;
    public final static int MAX_NUMBER_OF_POWER = 10;
    public final static int EDGE_FOR_LIGHT_POWER = 3;
    public final static int EDGE_FOR_MEDIUM_POWER = 7;

    public final static int LEFT_NUMBER_OF_SIDE = 1;
    public final static int CENTER_NUMBER_OF_SIDE = 2;
    public final static int RIGHT_NUMBER_OF_SIDE = 3;

    public final static int MIN_DIFF_FOR_WIN = 2;
    public final static int MIN_POINTS_FOR_WIN = 11;

    public final static int INCORRECT_SIDE = 0;

    public final static Object SYNCHRONIZED_OBJECT = new Object();

    private PingPongConstants() {
    }
}

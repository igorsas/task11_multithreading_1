package com.igor.model.pingPong;

import com.igor.controller.PingPongController;
import com.igor.model.pingPong.enums.PowerOfKick;
import com.igor.model.pingPong.enums.Side;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

import static com.igor.model.pingPong.PingPongConstants.*;
import static com.igor.model.Constants.*;
public class Game {
    private Logger logger = LogManager.getLogger(Game.class);
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private Player firstPlayer;
    private Player secondPlayer;

    public Game(Player firstPlayer, Player secondPlayer) {
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
    }

    public void doStep(Player player) {
        Player opponent = getOpponent(player);
        Side sideDefend = Side.CENTER;//default
        if (Objects.nonNull(opponent.getLastStep())) {
            sideDefend = getSide(DEFEND_STR);
        }
        Side side = getSide(KICK_STR);
        PowerOfKick powerOfKick = getPowerOfKick();
        if (checkBallReflection(sideDefend, opponent) && checkBallPower(powerOfKick, opponent)) {
            player.doStep(sideDefend, side, powerOfKick);
        } else {
            player.clearLastStep();
            opponent.plusPoint();
            PingPongController.view.out(opponent.getName() + SCORED);
            PingPongController.view.showScore();
        }
    }


    private boolean checkBallPower(PowerOfKick powerOfKick, Player opponent) {
        if (Objects.nonNull(opponent.getLastStep())) {
            if (powerOfKick == PowerOfKick.STRONG) {
                return opponent.getLastStep().getPowerOfKick().getPower() <= EDGE_FOR_MEDIUM_POWER;
            }
        }
        return true;
    }

    private boolean checkBallReflection(Side sideDefend, Player opponent) {
        if (Objects.nonNull(opponent.getLastStep())) {
            if (sideDefend == Side.CENTER) {
                return opponent.getLastStep().getSide() == sideDefend;
            } else {
                if (sideDefend == Side.RIGHT) {
                    return opponent.getLastStep().getSide() == Side.LEFT;
                }
                return opponent.getLastStep().getSide() == Side.RIGHT;
            }
        }
        return true;
    }

    private Player getOpponent(Player player) {
        if (player.equals(this.firstPlayer)) {
            return this.secondPlayer;
        }
        return this.firstPlayer;
    }

    private PowerOfKick getPowerOfKick() {
        int numberPowerOfKick = INCORRECT_SIDE;
        while (!checkNumberPowerOfKick(numberPowerOfKick)) {
            PingPongController.view.out(ASK_POWER_OF_KICK);
            numberPowerOfKick = getNumberFromConsole();
        }
        return findPowerOfKickByNumber(numberPowerOfKick);
    }

    private boolean checkNumberPowerOfKick(int numberPowerOfKick) {
        return numberPowerOfKick >= MIN_NUMBER_OF_POWER && numberPowerOfKick <= MAX_NUMBER_OF_POWER;
    }

    private PowerOfKick findPowerOfKickByNumber(int numberPowerOfKick) {
        if (numberPowerOfKick < EDGE_FOR_LIGHT_POWER) {
            return PowerOfKick.LIGHT;
        } else if (numberPowerOfKick <= EDGE_FOR_MEDIUM_POWER) {
            return PowerOfKick.MEDIUM;
        } else {
            return PowerOfKick.STRONG;
        }
    }

    private Side getSide(String typeSide) {
        int numberSideOfKick;
        do {
            PingPongController.view.out(typeSide + ASK_SIDE);
            numberSideOfKick = getNumberFromConsole();
        } while (!checkNumberSideOfKick(numberSideOfKick));
        return findSideByNumber(numberSideOfKick);
    }

    private Side findSideByNumber(int numberSideOfKick) {
        Side side = Side.CENTER;
        switch (numberSideOfKick) {
            case LEFT_NUMBER_OF_SIDE:
                side = Side.LEFT;
                break;
            case CENTER_NUMBER_OF_SIDE:
                side = Side.CENTER;
                break;
            case RIGHT_NUMBER_OF_SIDE:
                side = Side.RIGHT;
                break;
            default:
                break;
        }
        return side;
    }

    private boolean checkNumberSideOfKick(int numberSideOfKick) {
        return numberSideOfKick == LEFT_NUMBER_OF_SIDE
                || numberSideOfKick == CENTER_NUMBER_OF_SIDE
                || numberSideOfKick == RIGHT_NUMBER_OF_SIDE;
    }

    private int getNumberFromConsole() {
        int number = INCORRECT_SIDE;
        try {
            number = Integer.parseInt(reader.readLine());
        } catch (NumberFormatException e) {
            logger.error(NUMBER_FORMAT_EXCEPTION_MSG);
        } catch (IOException e) {
            logger.error(IOEXCEPTION_MSG);
            e.printStackTrace();
        }
        return number;
    }
}

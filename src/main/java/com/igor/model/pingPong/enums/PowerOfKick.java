package com.igor.model.pingPong.enums;

public enum PowerOfKick {
    LIGHT(1),
    MEDIUM(5),
    STRONG(10);

    private int power;

    PowerOfKick(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }
}

package com.igor.model.pingPong;

import com.igor.model.pingPong.enums.PowerOfKick;
import com.igor.model.pingPong.enums.Side;

import java.util.Objects;

public class Player {
    private String name;
    private int point;
    private Step lastStep;

    public Player(String name) {
        this.name = name;
        point = 0;
    }

    void doStep(Side sideDefend, Side side, PowerOfKick powerOfKick) {
        this.lastStep = new Step(sideDefend, side, powerOfKick);
    }

    public String getName() {
        return name;
    }

    public int getPoint() {
        return point;
    }

    Step getLastStep() {
        return lastStep;
    }

    void plusPoint() {
        this.point++;
    }

    void clearLastStep() {
        this.lastStep = null;
    }

    @Override
    public String toString() {
        return "Player:\n" +
                "Name: " + name +
                ", Points: " + point;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return getPoint() == player.getPoint() &&
                Objects.equals(getName(), player.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPoint());
    }
}

package com.igor.model.pingPong;

import com.igor.model.pingPong.enums.PowerOfKick;
import com.igor.model.pingPong.enums.Side;

public class Step {

    private Side sideDefend;
    private Side sideKick;
    private PowerOfKick powerOfKick;

    Step(Side sideDefend, Side side, PowerOfKick powerOfKick) {
        this.sideDefend = sideDefend;
        this.sideKick = side;
        this.powerOfKick = powerOfKick;
    }

    public Side getSideDefend() {
        return sideDefend;
    }

    Side getSide() {
        return sideKick;
    }

    PowerOfKick getPowerOfKick() {
        return powerOfKick;
    }
}

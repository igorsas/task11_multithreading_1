package com.igor.model.fibonacci;

public class FibonacciConstants {
    public final static long FIRST_FIBONACCI_NUMBER = 0;
    public final static long SECOND_FIBONACCI_NUMBER = 1;
    public final static int MAX_NUMBER_OF_FIBONACCI_NUMBERS = 30;

    public final static int MIN_COUNT_OF_THREADS = 1;
    public final static int INCORRECT_COUNT_OF_THREADS = 0;

    public final static String FIBONACCI_NUMBERS = "Fibonacci numbers: ";
    public final static String ASK_COUNT_OF_THREADS = "Please enter count of threads: ";
    private FibonacciConstants(){}
}

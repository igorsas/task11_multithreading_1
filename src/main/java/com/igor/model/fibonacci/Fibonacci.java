package com.igor.model.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import static com.igor.model.Constants.*;
import static com.igor.model.fibonacci.FibonacciConstants.*;
public class Fibonacci {
    Logger logger = LogManager.getLogger(Fibonacci.class);
    List<Thread> threads;
    private List<Long> fibonacciNumbers;

    public Fibonacci(int countThreads){
        threads = new ArrayList<>(countThreads);
        fibonacciNumbers = new ArrayList<>();
        for (int i = 0; i < countThreads; i++) {
            threads.add(getThread(new Random().nextInt(MAX_NUMBER_OF_FIBONACCI_NUMBERS)));
        }
    }

    private Thread getThread(int countFibonacciNumbers){
        return new Thread(() -> {
            if(fibonacciNumbers.isEmpty()) {
                fibonacciNumbers.add(FIRST_FIBONACCI_NUMBER);
                fibonacciNumbers.add(SECOND_FIBONACCI_NUMBER);
            }
            long firstNumber = fibonacciNumbers.get(fibonacciNumbers.size()-1);
            long secondNumber = fibonacciNumbers.get(fibonacciNumbers.size()-2);
            for (int i = 2; i <= countFibonacciNumbers; i++) {
                secondNumber += firstNumber;
                firstNumber = secondNumber - firstNumber;
                fibonacciNumbers.add(secondNumber);
            }
        });
    }

    public List<Long> getFibonacciNumbers() {
        return fibonacciNumbers;
    }


    public void startAllThreads() {
        for(Thread thread : threads) {
            thread.start();
        }
    }

    public void joinToAllThreads() {
        for(Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                logger.error(INTERRUPTED_EXCEPTION_MSG);
                e.printStackTrace();
            }
        }
    }
}

package com.igor.model;

public class Constants {
    public final static String IOEXCEPTION_MSG = "Incorrect input data IOException";
    public final static String INTERRUPTED_EXCEPTION_MSG = "InterruptedException: ";
    public final static String NUMBER_FORMAT_EXCEPTION_MSG = "NumberFormatException: ";

    public final static String EMPTY_STRING = "";
    public final static String GAP = " ";
    public final static String COLON_STRING = " : ";
}

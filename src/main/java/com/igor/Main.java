package com.igor;

import com.igor.controller.Controller;
import com.igor.controller.FibonacciController;

public class Main {
    public static void main(String[] args) {
        Controller controller = new FibonacciController();
        controller.execute();
//        Controller pingPongController = new PingPongController();
//        pingPongController.execute();
    }
}
